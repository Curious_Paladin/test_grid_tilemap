﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

// Перечисляем типы тайлов
public enum TileType { Grass, Wall, Water };
public class GridManagerBehaviour : MonoBehaviour {
  // Объект tilemap
  [SerializeField] private Tilemap tilemap;
  // Массив тайлов
  [SerializeField] private Tile[] tiles;
  // Объект камера
  [SerializeField] private Camera worldCamera;
  // Маска слоя
  [SerializeField] private LayerMask layerMask;
  // Префаб для CellLabel
  [SerializeField] private GameObject cellLabelPrefab;
  //
  [SerializeField] private bool enableDebug;
  // Тип тайла
  private TileType tileType;

  /**
  * Start
  **/
  private void Start() {
    CheckScriptObjectsExist();

    if (enableDebug & (cellLabelPrefab != null)) {
      ShowTileDebugLabel();
    } else if (cellLabelPrefab == null) {
      Debug.LogError("cellLabelPrefab is not set");
    }
  }

  /**
  * Update
  **/
  private void Update() {
    GetMouseClick();
  }

  /**
  * Реагируем на клики мыши
  **/
  private void GetMouseClick() {
    // Если нажата левая кнопка мыши
    if (Input.GetMouseButtonDown(0)) {
      // Получаем точку клика на игровом поле
      Vector3 screenToWorldPoint = worldCamera.ScreenToWorldPoint(Input.mousePosition);

      /**
      * Я долго искал, как правильно настроить получение кликов именно по tilemap
      * Без этого тайлы ставятся на всё, включая меню и кнопки 
      * Пока еще не очень разобрался в работе Raycast, читайте документацию =)
      **/
      RaycastHit2D hit = Physics2D.Raycast(
        // Начальная точка процирования луча
        screenToWorldPoint,
        // Конечная точка
        Vector2.zero,
        // Длина луча
        Mathf.Infinity,
        // Маска слоя, который нам нужен - Tilemap
        layerMask
      );

      // Если есть перечение со слоем Tilemap, реагируем на это
      if (hit.collider != null) {
        // Получаем номер тайла, который будем менять
        Vector3Int mouseClickPosition = tilemap.WorldToCell(screenToWorldPoint);
        // Меняем тайл на тот, что выбран в данный момент
        tilemap.SetTile(mouseClickPosition, tiles[(int)tileType]);
      }
    }
  }

  /**
  * Выбор типа тайла
  **/
  public void ChangeTyleType(TileButtonBehaviour button) {
    tileType = button.currentTileType;
  }

  /**
  * Проверяем, подключены ли все объекты к скрипту
  **/
  private void CheckScriptObjectsExist() {
    if (tilemap == null) {
      Debug.LogError("tilemap is not set");
    }

    if (worldCamera == null) {
      Debug.LogError("worldCamera is not set");
    }

    if (tiles.Length == 0) {
      Debug.LogError("tiles are not set");
    }
  }

  /**
  * Показать лейблы для тайлов
  **/
  private void ShowTileDebugLabel() {
    // Проходим циклом по позициям всех тайлов в tilemap
    foreach (var tilePosition in tilemap.cellBounds.allPositionsWithin) {   
      // Конвертируем полученную позицию в Vector3Int
      Vector3Int localTilePosition = new Vector3Int(tilePosition.x, tilePosition.y, tilePosition.z);

      // Если тайл существует, добавляем его лейбл в игру
      if (tilemap.HasTile(localTilePosition)) {
        var labelGameobject = Instantiate(
          cellLabelPrefab, 
          new Vector3(localTilePosition.x + 0.5f, localTilePosition.y + 0.5f, localTilePosition.z), 
          Quaternion.identity
        );

        // Получаем компонент TextMesh
        TextMesh labelTextMesh = labelGameobject.GetComponent<TextMesh>();
        // Конвертируем Vector3 в Vector2Int
        Vector2Int localTilePositionInt = new Vector2Int(Mathf.FloorToInt(localTilePosition.x), Mathf.FloorToInt(localTilePosition.y));
        // Выбираем текст для отображения
        labelTextMesh.text = localTilePositionInt.ToString();
      }
    }
  }
}
