﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileButtonBehaviour : MonoBehaviour {
  // Устанавливаем тип тайла из списка TileType: Grass, Wall, Water
  [SerializeField] TileType tileType;

  // Геттер для получения выбранного типа тайла
  public TileType currentTileType {
    get {
      return tileType;
    }
  }
}