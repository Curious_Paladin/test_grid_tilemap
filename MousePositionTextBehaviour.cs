﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MousePositionTextBehaviour : MonoBehaviour {
  // Игровая камера
  [SerializeField] private Camera worldCamera;
  // Текстовый компонент, в котором мы будем менять значение текста
  private Text textComponent;

  /**
  * Start
  **/
  private void Start() {
    // Поулчаем текстовый компонент
    textComponent = GetComponent<Text>();

    // Если textComponent или worldCamera отсцтствуют, то мы получим сообщение об ошибке
    if (textComponent == null) {
      Debug.LogError("textComponent is missed");
    }

    if (worldCamera == null) {
      Debug.LogError("worldCamera is missed");
    }
  }

  /**
  * Update
  **/
  private void Update() {
    UpdateComponentText();
  }

  /**
  * Обновляем текст, чтобы видеть текущие координаты курсора
  **/
  private void UpdateComponentText() {
    if (textComponent != null & worldCamera!= null) {
      // Координаты положения курсора на экране (учитывается весь экран, даже за пределами Unity)
      Vector3 inputMousePosition = Input.mousePosition;
      // Координаты положения курсора на внутри игрового мира
      Vector3 mouseWorldPosition = worldCamera.ScreenToWorldPoint(inputMousePosition);
     
      /**
      * Обновляем текст для отображения в игре
      * MWP = mouseWorldPosition
      * IMP = inputMousePosition
      **/
      textComponent.text = $"MWP\n {mouseWorldPosition.ToString()}\n IMP\n {inputMousePosition.ToString()}";
    }
  }
}
